# Set the host name for URL creation
SitemapGenerator::Sitemap.default_host = 'http://kavigator.ru'

SitemapGenerator::Sitemap.create do
  NewsSource.active.each do |ns|
    add articles_path(source_id: ns.id), changefreq: 'hourly', priority: 0.6
    ns.articles.published.find_each(batch_size: 1000) do |a|
      add article_path(a), changefreq: 'monthly', priority: 0.5, lastmod: a.updated_at
    end
  end
  Trend.all.each do |trend|
    add articles_path(trend_id: trend.id), changefreq: 'hourly', priority: 0.7
  end
end
