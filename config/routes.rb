Kavigator::Application.routes.draw do
  devise_for :users
  resources :articles, only: [:index, :show] do
    get :feed, on: :collection, defaults: { format: :atom }
  end

  namespace :admin do
    resources :trends
    resources :keywords
    resources :users
    resources :news_sources do
      post :clear, on: :member
    end
    resources :articles do
      get :feed, on: :collection
    end
    root to: 'articles#index'
  end
  root to: 'articles#index', defaults: { archive: true }
end
