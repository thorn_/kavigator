authorization do
  role :guest do
    has_permission_on :articles, to: [:read]
    has_permission_on :devise_registrations, to: :manage
    has_permission_on :devise_sessions, to: :manage
  end

  role :user do
    includes :guest
  end

  role :moderator do
    includes :user
    has_permission_on :from_tracking, to: [:be_free]
    has_permission_on :articles, :admin_articles, to: [:manage]
    has_permission_on :admin_trends, to: [:manage]
    has_permission_on :admin_news_sources, to: [:manage]
  end

  role :admin do
    includes :moderator
    has_permission_on :admin_users, to: [:manage]
    has_permission_on :admin_keywords, to: [:manage]
  end
end

privileges do
  privilege :manage, includes: [:create, :read, :update, :delete, :clear]
  privilege :read,   includes: [:index, :show, :feed]
  privilege :create, includes: :new
  privilege :update, includes: :edit
  privilege :delete, includes: :destroy
end
