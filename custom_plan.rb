# -*- encoding: utf-8 -*-
require 'zeus/rails'

class CustomPlan < Zeus::Rails
  def test
    require 'simplecov'
    # SimpleCov.start
    # SimpleCov.start 'rails' if using RoR
    SimpleCov.start 'rails'
    # require all ruby files
    Dir["#{Rails.root}/app/**/*.rb"].each { |f| load f }

    # run the tests
    ENV['GUARD_RSPEC_RESULTS_FILE'] = 'tmp/guard_rspec_results.txt' # can be anything matching Guard::RSpec :results_file option in the Guardfile
    super
  end
end

Zeus.plan = CustomPlan.new
