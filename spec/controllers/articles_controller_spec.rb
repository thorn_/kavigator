require 'spec_helper'

describe ArticlesController do
  describe "GET 'index'" do
    let(:article1) { FactoryGirl.create(:article, published: true) }
    let(:article2) { FactoryGirl.create(:article, published: true) }

    it 'should be ok' do
      get :index
      expect(response).to be_ok
    end

    it 'should fetch all articles' do
      get :index
      expect(assigns(:articles)).to include(article1, article2)
    end

    it 'should get number of news that are created today' do
      article1
      article2
      get :index
      expect(assigns(:today_news_count)).to eq(2)
    end
  end

  describe "GET 'feed'" do
    it 'redirect to atom format' do
      get :feed, format: :rss
      expect(response).to redirect_to(feed_articles_path(format: :atom))
    end

    it 'should contain articles info in feed' do
      article = FactoryGirl.create(:article, published: true)
      get :feed, format: :atom
      expect(assigns(:articles)).to include(article)
      expect(response).to be_ok
    end
  end

  describe "GET 'show'" do
    let(:article) { FactoryGirl.create(:article) }

    it 'should have 200 response' do
      get :show, id: article.id
      expect(response).to be_ok
    end

    it 'should fetch appropriate article' do
      get :show, id: article.id
      expect(assigns(:article)).to eq(article)
    end
  end
end
