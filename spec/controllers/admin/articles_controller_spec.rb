require 'spec_helper'

describe Admin::ArticlesController do
  before(:each) do
    sign_in FactoryGirl.create(:user)
  end

  describe "PATCH 'update'" do
    let(:article) { FactoryGirl.create(:article, sticky: false, published: false) }

    it 'should update article' do
      patch :update, id: article.id, article: { sticky: true, published: true }
      article.reload
      expect(article.sticky).to eq(true)
      expect(article.published).to eq(true)
    end
  end

  describe "DELETE 'destroy'" do
    before(:each) do
      @article = FactoryGirl.create(:article)
    end
    it 'should be ok' do
      delete :destroy, id: @article.id
    end

    it 'should find given article' do
      delete :destroy, id: @article.id
      expect(assigns(:article)).to eq(@article)
    end

    it 'should delete that article' do
      expect do
        delete :destroy, id: @article.id
      end.to change { Article.count }.by(-1)
    end
  end
end
