# -*- encoding: utf-8 -*-
require 'spec_helper'

describe Admin::KeywordsController do
  before(:each) { sign_in FactoryGirl.create(:user) }
  describe "GET 'new'" do
    it 'returns http success' do
      get :new
      expect(response).to be_success
    end
  end

  describe "GET 'index'" do
    it 'should be success' do
      get :index
      expect(response).to be_success
    end
  end

  describe "POST 'create'" do
    before(:each) do
      @attr = { name: "cat1\ncat2" }
    end

    describe 'success' do
      it 'should create an instance of keyword' do
        expect do
          post :create, keyword: @attr
          expect(response).to redirect_to(admin_keywords_path)
        end.to change(Keyword, :count).by(2)
      end
    end

    describe 'failure' do
      it 'should not create an instance of keyword without name' do
        expect do
          post :create, keyword: @attr.merge(name: '')
          expect(response).to render_template('keywords/new')
        end.not_to change { Keyword.count }
      end
    end
  end

  describe "PUT 'update'" do
    describe 'success' do
      before(:each) do
        @attr = { name: 'Коммерсант',
                  keyword_type: 'required' }
        @keyword = Keyword.create!(name: 'me', keyword_type: 'required')
      end

      it 'should update a keyword' do
        put :update, id: @keyword.id, keyword: @attr.merge(name: 'Kommersant')
        @keyword.reload
        expect(@keyword.name).to eq('Kommersant')
      end

      it 'should redirect to index' do
        put :update, id: @keyword.id, keyword: @attr.merge(name: 'Kommersant')
        response.should redirect_to admin_keywords_path
      end

      it 'should have a success flash message' do
        put :update, id: @keyword.id, keyword: @attr
        flash[:success].should =~ /ключевое слово успешно изменено/i
      end
    end

    describe 'failure' do
      before(:each) do
        @attr = { name: '', keyword_type: 'not_included' }
        @source = Keyword.create!(name: 'Коммерсант',
                                  keyword_type: 'required')
      end

      it "should render 'edit' action" do
        put :update, id: @source.id, keyword: @attr
        response.should render_template('keywords/edit')
      end

      it 'should not update keyword' do
        put :update, id: @source.id, keyword: @attr
        @source.reload
        @source.name.should == 'Коммерсант'
      end

      it 'should have a failure flash' do
        put :update, id: @source.id, keyword: @attr
        flash[:error].should =~ /ключевое слово не изменено/i
      end
    end
  end

  describe "DELETE 'destroy'" do
    it 'should destroy keyword' do
      @keyword = Keyword.create!(name: 'cat2')
      lambda do
        delete :destroy, id: @keyword.id
        response.should redirect_to(admin_keywords_path)
      end.should change(Keyword, :count).by(-1)
    end
  end
end
