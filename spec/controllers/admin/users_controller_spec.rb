# -*- encoding: utf-8 -*-
require 'spec_helper'

describe Admin::UsersController do
  before(:each) do
    @user = FactoryGirl.create(:user)
    sign_in @user
  end

  describe "GET 'index'" do
    it 'returns http success' do
      get 'index'
      response.should be_success
    end
  end

  describe "DELETE 'destroy'" do
    it 'should destroy given user' do
      lambda do
        delete :destroy, id: @user.id
        response.should redirect_to(admin_users_path)
      end.should change(User, :count).by(-1)
    end

    it 'should have a flash message' do
      delete :destroy, id: @user.id
      flash[:success].should =~ /успешно удален/i
    end
  end

  describe "PUT 'update'" do
    before(:each) do
      put :update, id: @user.id, user: { role: 'administrator', email: FactoryGirl.generate(:email) }
    end

    it 'should update users credentials' do
      old_email = @user.email
      @user.reload
      @user.role.should == 'administrator'
      @user.email.should_not == old_email
    end

    it 'should redirecto to users path' do
      response.should redirect_to(admin_users_path)
    end

    it 'should have appropriate flash message' do
      flash[:success].should =~ /успешно/i
    end
  end

  describe 'POST "create"' do
    let!(:attr) do
      {
        email: FactoryGirl.generate(:email),
        password: 'password',
        password_confirmation: 'password',
        role: 'admin'
      }
    end
    it 'redirects to list if all is okay' do
      post :create, user: attr
      expect(response).to redirect_to(admin_users_path)
    end

    it 'renders new otherwise' do
      post :create, user: attr.merge(role: nil)
      expect(response).to render_template(:new)
    end
  end

  describe "GET 'new'" do
    it 'should be ok' do
      get :new
      response.should be_ok
    end
  end
end
