# -*- encoding: utf-8 -*-
FactoryGirl.define do
  sequence :url do |n|
    "http://www.example#{n}.com/feed.xml"
  end

  factory :article do
    title 'LOREM IPSUM'
    text 'Lorem ipsum dolor sit amet'
    summary 'summary'
    img_url nil
    news_source
    trend
    ancestry nil
    published false
    sticky false
    shared false
    archive false
  end

  factory :keyword do
    name 'Name'
    keyword_type 'required'
  end

  factory :news_source do
    name 'Source1'
    url
    webpage 'http://example.com'
    active true
    greedy false
    image_url ''
    description 'news_source description'
  end

  factory :trend do
    name 'trend'
    description 'trend description'
    active true
  end

  factory :visited_page do
    url
  end

  factory :user do
    email
    password 'password'
    password_confirmation 'password'
    role 'user'
  end

  sequence :email do |n|
    "example#{n}@mail.com"
  end
end
