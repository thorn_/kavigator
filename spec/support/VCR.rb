VCR.configure do |config|
  config.hook_into :webmock
  config.cassette_library_dir = Rails.root.join('spec', 'fixtures', 'cassetes')
end
