# -*- encoding: utf-8 -*-
require 'spec_helper'
require 'ostruct'

describe ApplicationHelper do
  include ActiveSupport::Testing::TimeHelpers
  describe 'methods' do
    it 'should return date in russian date format if year is not currrent' do
      result = published_date(OpenStruct.new(created_at: DateTime.now - 366.days))
      expect(result).to match(/\d{4}\.\d{2}\.\d{2}/)
    end

    it 'should return date in words' do
      result = published_date(OpenStruct.new(created_at: DateTime.now))
      expect(result).to match(/назад/)
    end

    it 'should return date and month name if year is current' do
      result = published_date(OpenStruct.new(created_at: DateTime.now - 4.days))
      expect(result).to match(/\d{1,2}.*\w+/)
    end

    it 'should allow additional javascript in production' do
      allow(self).to receive('permitted_to?').and_return(false)
      Rails.env = 'production'
      expect(additional_javascript_permitted?).to eq(true)
    end

    it 'should not load javascript if user is admin' do
      allow(self).to receive('permitted_to?').and_return(true)
      Rails.env = 'production'
      expect(additional_javascript_permitted?).to eq(false)
    end

    it 'should not load javascript if environment is not' do
      allow(self).to receive('permitted_to?').and_return(false)
      Rails.env = 'test'
      expect(additional_javascript_permitted?).to eq(false)
    end
  end

  describe 'title maker' do
    let(:default_title) { 'Новости Дагестана | Кавигатор' }

    it 'should return default title there is noting left' do
      expect(site_title).to eq(default_title)
    end

    it 'should return title + default title if there is variable' do
      @title = 'new title'
      expect(site_title).to eq("#{@title} | #{default_title}")
    end

    it 'should add article title to default one' do
      @article = FactoryGirl.create(:article, title: 'Article Title')
      expect(site_title).to eq("#{@article.title} | #{default_title}")
    end
  end

  describe 'background maker' do
    it 'chooses day folder during the day' do
      (8..16).each do |i|
        travel_to Time.new(2015, 10, 10, i, 0) do
          expect(background_folder).to eq('day')
        end
      end
    end

    it 'chooses evening folder during the evening' do
      (17..22).each do |i|
        travel_to Time.new(2015, 10, 10, i, 0) do
          expect(background_folder).to eq('evening')
        end
      end
    end

    it 'chooses night folder during the night' do
      [23, 0, 1, 2, 3, 4, 5, 6, 7].each do |i|
        travel_to Time.new(2015, 10, 10, i, 0) do
          expect(background_folder).to eq('night')
        end
      end
    end

    it 'returns random background url' do
      expect(random_background_url).to match(%r{\Abackgrounds\/(day|evening|night)/\d\.jpg\z})
    end
  end
end
