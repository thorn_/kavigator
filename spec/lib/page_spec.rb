# -*- encoding: utf-8 -*-
require 'spec_helper'
require 'nokogiri'

describe Page do
  let(:url) { 'http://www.riadagestan.ru/news/tarumovskiy_rayon/v_kizlyare_proshlo_pervenstvo_severnoy_zony_dagestana_po_mini_futbolu/' }
  describe 'without processing' do
    before(:each) do
      VCR.use_cassette 'page_requests' do
        @page = Page.new(url)
      end
    end

    it 'should get first image' do
      expect(@page.image).to eq('//riadagestan.ru.images.1c-bitrix-cdn.ru/bitrix/templates/ria_dagestan/img/logotip2.jpg?1369338465')
    end

    it 'should pick summary by first paragraph' do
      expect(@page.summary).to match(/РеспубликанскоеИнформационноеАгентство/)
    end

    it 'should have url accessor' do
      expect(@page.url).to eq(url)
    end
  end

  describe 'with processing' do
    before(:each) do
      VCR.use_cassette 'page_requests' do
        @page = Page.new(url)
      end
      process_page
    end

    def process_page
      host = URI.parse(url).host
      find = '#qaz, .pikachoose'
      ignored = 'noignore'

      @page.process!(find_helper: find, host: host, ignored_helper: ignored)
    end

    it 'should get right summary' do
      expect(@page.summary).not_to match(/Республиканское информационное агентство/)
    end

    it 'should get right image' do
      expect(@page.image).to eq('//riadagestan.ru.images.1c-bitrix-cdn.ru/upload/fotonews/result_image_small012616.png?1388314917')
    end

    it 'should return blank text if page is not accessible' do
      expect(Page.new('invalid url').text).to eq('')
    end

    it 'returns a page after processing' do
      expect(process_page.class).to eq(Page)
    end
  end

  describe 'check_page method' do
    let(:long_text) do
      "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
      tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
      quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
      consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
      cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
      proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
    end

    before(:each) do
      VCR.use_cassette 'page_requests' do
        @page = Page.new(url)
      end
      FactoryGirl.create(:keyword, keyword_type: 'required', name: 'required')
      FactoryGirl.create(:keyword, keyword_type: 'not_included', name: 'not_included')
    end

    it 'should return true if page has one of required words and none of not required' do
      allow(@page).to receive(:text).and_return('required ' + long_text)
      expect(@page.check).to eq(true)
    end

    it 'should return false if there are no required words' do
      allow(@page).to receive(:text).and_return(long_text)
      expect(@page.check).to eq(false)
    end

    it 'should return false if there are some not required words' do
      allow(@page).to receive(:text).and_return('not_included required ' + long_text)
      expect(@page.check).to eq(false)
    end

    it 'should return false if there are not enough words' do
      allow(@page).to receive(:text).and_return('required')
      expect(@page.check).to eq(false)
    end
  end

end
