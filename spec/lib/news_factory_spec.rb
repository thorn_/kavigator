# -*- encoding: utf-8 -*-
require 'spec_helper'
require 'ostruct'

describe NewsFactory do
  let(:url) { 'http://www.riadagestan.ru/news/tarumovskiy_rayon/v_kizlyare_proshlo_pervenstvo_severnoy_zony_dagestana_po_mini_futbolu/' }

  describe 'get_summary method' do
    it 'should get summary' do
      pending "Test this"
    end
  end

  describe 'create article method' do
    let(:page) { OpenStruct.new(image: 'http://www.image.com', text: 'text', url: 'http://www.example.com/page', summary: 'page summary') }
    let(:entry) { OpenStruct.new(title: 'title', summary: 'entry summary') }
    let(:news_source) { FactoryGirl.create(:news_source)  }

    it 'should fill article from entry and page' do
      article = NewsFactory.create_article(page, entry, news_source)
      expect(article.url).to eq(page.url)
      expect(article.summary).to eq(entry.summary)
    end
  end

  describe 'process page and get news method' do
    let(:news_source) { FactoryGirl.create(:news_source, url: 'http://www.example.com', find_helper: '#qaz, .pikachoose', ignore_helper: 'noignore') }
    before(:each) do
      VCR.use_cassette 'page_requests' do
        @page = Page.new(url)
      end
      entries = OpenStruct.new(entries: [OpenStruct.new(url: url, title: 'title')])
      allow(Feedjira::Feed).to receive(:fetch_and_parse).and_return(entries)
      FactoryGirl.create(:keyword, name: 'в', keyword_type: 'required')
      allow(Page).to receive(:new).and_return(@page)
    end

    it 'should process page' do
      expect(NewsFactory.process_page('url', 'find', 'ignore')).to eq(@page)
    end

    it 'should get some feeds' do
      expect(Feedjira::Feed).to receive(:fetch_and_parse)
      NewsFactory.get_news([news_source])
    end

    it 'should create article' do
      expect do
        NewsFactory.get_news([news_source])
      end.to change { Article.count }.by(1)
    end
    it 'should mark this url as visited' do
      expect do
        NewsFactory.get_news([news_source])
      end.to change { VisitedPage.count }.by(1)
    end
  end
end
