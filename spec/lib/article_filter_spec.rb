require 'spec_helper'

describe ArticleFilter do
  let!(:trend) { FactoryGirl.create(:trend) }
  let!(:source1) { FactoryGirl.create(:news_source) }
  let!(:source2) { FactoryGirl.create(:news_source) }

  before(:each) do
    @article1 = FactoryGirl.create(:article,
                                   news_source: source1,
                                   published: true)
    @article2 = FactoryGirl.create(:article,
                                   news_source: source2,
                                   trend: trend,
                                   published: true)
    @article3 = FactoryGirl.create(:article,
                                   news_source: source2,
                                   trend: trend,
                                   published: false)
    @article4 = FactoryGirl.create(:article,
                                   news_source: source2,
                                   archive: true,
                                   published: true)
  end

  it 'should return plain articles if there are no params given' do
    ArticleFilter.filter({}).should include(@article1, @article2)
  end

  it 'should return unpublished articles if I ask' do
    ArticleFilter.filter({}, false).should include(@article3)
  end

  it 'should return articles from news source' do
    articles = ArticleFilter.filter(source_id: source1.id)
    expect(articles).to include(@article1)
    expect(articles).not_to include(@article2)
  end

  it 'should return articles from news source' do
    articles = ArticleFilter.filter(trend_id: trend.id)
    expect(articles).to include(@article2)
    expect(articles).not_to include(@article1)
  end

  it 'should return all articles, published or not' do
    articles = ArticleFilter.filter(archive: true)
    expect(articles).to include(@article4, @article3, @article2, @article1)
  end

  it 'should not include articles from archive if is not asked' do
    ArticleFilter.filter({}).should_not include(@article4)
    ArticleFilter.filter(source_id: source1.id).should_not include(@article4)
  end
end
