# -*- encoding: utf-8 -*-
require 'spec_helper'

describe Checker::BaseChecker do
  before(:each) do
    @ch = Checker::BaseChecker.new('the sandwich on the wall')
  end

  it 'should raise description when abstract method was called' do
    expect { @ch.check }.to raise_error(RuntimeError)
  end

  it 'should return count of words in text' do
    @ch.word_on_text('sandwich').should eq(1)
    @ch.word_on_text('the').should eq(2)
  end

  it 'should return right words count' do
    @ch.word_count.should eq(2)
  end
end

describe Checker::RequiredWordsChecker do
  it 'should return true if there is even 1 required word on page' do
    FactoryGirl.create(:keyword, keyword_type: 'required', name: 'sandwich')
    checker = Checker::RequiredWordsChecker.new('what was that sandwich')
    expect(checker.check).to eq(true)
  end

  it 'should return false if there is no required words on text' do
    FactoryGirl.create(:keyword, keyword_type: 'required', name: 'sandwich')
    checker = Checker::RequiredWordsChecker.new('what was that')
    checker.check.should == false
  end
end

describe Checker::ArticleLengthChecker do
  before(:each) do
    @checker = Checker::ArticleLengthChecker.new('some text about sandwich')
  end

  it 'should return false if there is less words on article' do
    @checker.check(length: 40).should == false
  end

  it 'should return true if there is enough words on text' do
    @checker.check(length: 1).should == true
  end
end

describe Checker::ExcludedWordsChecker do
  it 'should return true if there is even 1 no required word on page' do
    FactoryGirl.create(:keyword, keyword_type: 'not_included', name: 'sandwich')
    checker = Checker::ExcludedWordsChecker.new('what was that sandwich')
    checker.check.should == false
  end

  it 'should return false if there is no required words on text' do
    FactoryGirl.create(:keyword, keyword_type: 'not_included', name: 'sandwich')
    checker = Checker::ExcludedWordsChecker.new('what was that')
    checker.check.should == true
  end
end
