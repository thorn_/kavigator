# -*- encoding: utf-8 -*-
require 'spec_helper'

describe VisitedPage do
  let(:attr) { { url: FactoryGirl.generate(:url) } }

  it 'should create a record with valid attributes' do
    expect { VisitedPage.create(attr) }.to change { VisitedPage.count }.by(1)
  end

  describe 'associations' do
    it { should belong_to(:news_source) }
  end

  describe 'validations' do
    it { should validate_presence_of(:url) }
  end
end
