# -*- encoding: utf-8 -*-
require 'spec_helper'

describe Article do
  let(:news_source) { FactoryGirl.create(:news_source) }
  let(:attr) do
    {
      title: 'lorem',
      text: '<p>Lorem ipsum dolor sit amet, consectetur</p>',
      news_source_id: news_source.id
    }
  end

  describe 'with valid attributes' do
    it 'should create an article' do
      expect { Article.create!(attr) }.to change { Article.count }.by(1)
    end

    it 'should be unpublished unshared and unsticky' do
      art = Article.create!(attr)
      expect(art.published).to eq(false)
      expect(art.shared).to eq(false)
      expect(art.sticky).to eq(false)
    end
  end

  describe 'associations' do
    it { should belong_to(:trend) }
    it { should belong_to(:news_source) }
  end

  describe 'scopes' do
    let(:article) { Article.create(attr) }

    it 'should have a published scope' do
      expect(Article.published).not_to include(article)
      article.update_attributes(published: true)
      expect(Article.published).to include(article)
    end

    it 'should have a unpublished scope' do
      expect(Article.unpublished).to include(article)
      article.update_attributes(published: true)
      expect(Article.unpublished).not_to include(article)
    end
  end

  describe 'validations' do
    it { should validate_presence_of(:title) }
    it { should validate_presence_of(:text) }
    it { should validate_presence_of(:news_source_id) }
  end
end
