require 'spec_helper'

RSpec.describe FeedProgress, type: :model do
  it 'initializes instance with give value' do
    progress = FeedProgress.start(100)
    expect(progress.total).to eq(100)
    expect(progress.current).to eq(0)
    expect(progress.active).to eq(true)
  end

  it 'marks as finished' do
    progress = FeedProgress.start(100)
    expect do
      progress.finish
    end.to change { progress.active }.from(true).to(false)
  end

  it 'calculates progress' do
    progress = FeedProgress.start(100, 30)
    expect(progress.progress).to eq(30)
  end

  it 'updates progress' do
    progress = FeedProgress.start(100, 30)
    progress.update_progress(50)
    expect(progress.progress).to eq(50)
  end
end
