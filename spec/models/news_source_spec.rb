# -*- encoding: utf-8 -*-
require 'spec_helper'

describe NewsSource do
  let(:attr) do
    {
      name: 'Коммерсант',
      webpage: 'http://www.kommersant.ru',
      url: 'http://www.feeds.kommersant.ru/RSS_Export/RU/daily.xml',
      description: 'description'
    }
  end

  it 'should create an instance with valid attributes' do
    expect do
      NewsSource.create(attr)
    end.to change { NewsSource.count }.by(1)
  end

  describe 'associations' do
    it { should have_many(:articles).dependent(:destroy) }
    it { should have_many(:visited_pages).dependent(:destroy) }
  end

  describe 'validations' do
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:url) }

    it 'validates uniqueness of url' do
      url = 'https://www.example.com/feed.xml'
      FactoryGirl.create(:news_source, url: url)
      expect(NewsSource.new(attr.merge(url: url))).not_to be_valid
    end

    it { should allow_value('http://www.example.org').for(:url) }
    it { should_not allow_value('http://exampleorg').for(:url) }
    it { should_not allow_value('ftp://exampleorg').for(:url) }
    it { should_not allow_value('ftp://exampleorg').for(:webpage) }
    it { should_not allow_value('http://exampleorg').for(:webpage) }
  end

  describe 'scopes' do
    it 'should get active sources only' do
      url = FactoryGirl.generate(:url)
      source1 = NewsSource.create!(attr)
      source2 = NewsSource.create!(attr.merge(active: false, url: url))
      NewsSource.active.should include(source1)
      NewsSource.active.should_not include(source2)
    end
  end

  describe 'clear' do
    let!(:source) { FactoryGirl.create(:news_source) }
    let!(:article) { FactoryGirl.create(:article, news_source: source) }
    let!(:visited_page) { FactoryGirl.create(:visited_page, news_source: source) }

    it 'deletes all articles and visited pages that belongs to current news source' do
      source.clear
      expect(source.articles.count).to eq(0)
      expect(source.visited_pages.count).to eq(0)
    end
  end
end
