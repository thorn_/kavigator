require 'spec_helper'

describe User do
  let(:attr) { { email: FactoryGirl.generate(:email), password: 'password' } }

  it 'should create a record with valid attributes' do
    expect { User.create!(attr) }.to change { User.count }.by(1)
  end

  describe 'validations' do
    it { should validate_presence_of(:role) }
  end

  describe 'methods' do
    let!(:user) { User.create!(attr) }

    it 'should return user role' do
      user.role_symbols.should == [:user]
    end

    it 'should return guest if user has no role' do
      user.role = nil
      user.role_symbols.should == [:guest]
    end

    it 'should return online users' do
      expect(user.online?).to be(true)
    end

    it "should return user's role" do
      expect(user.user_role).to eq('Пользователь')
    end
  end
end
