# -*- encoding: utf-8 -*-
require 'spec_helper'

describe Keyword do
  describe 'successful creation' do
    it 'should create a keyword with valid attributes' do
      expect do
        Keyword.create!(name: 'Keyword', keyword_type: 'required')
      end.to change { Keyword.count }.by(1)
    end
  end

  describe 'validations' do
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:keyword_type) }
  end

  describe 'scopes' do
    it 'should have a required keywords scope' do
      required = Keyword.create!(name: 'required', keyword_type: 'required')
      expect(Keyword.required).to include(required)
    end

    it 'should have a not included keywords scope' do
      not_included = Keyword.create!(
        name: 'not_included',
        keyword_type: 'not_included'
      )
      expect(Keyword.not_included).to include(not_included)
    end
  end
end
