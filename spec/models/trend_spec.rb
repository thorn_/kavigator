# -*- encoding: utf-8 -*-
require 'spec_helper'

describe Trend do
  let(:attr) do
    { name: 'Trend1', active: false, description: 'Trend1 description' }
  end

  it 'should create a valid trend' do
    expect { Trend.create!(attr) }.to change { Trend.count }.by(1)
  end

  describe 'validations' do
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:description) }
  end

  describe 'associations' do
    it { should have_many(:articles) }
  end

  describe 'scopes' do
    let!(:active_trend) { FactoryGirl.create(:trend, active: true) }
    let!(:inactive_trend) { FactoryGirl.create(:trend, active: false) }

    it 'returns only active trends' do
      expect(Trend.active).to include(active_trend)
      expect(Trend.active).not_to include(inactive_trend)
    end
  end
end
