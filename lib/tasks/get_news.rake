namespace :articles do
  desc 'Factory that produces news'
  task get_news: :environment do
    NewsFactory.get_news(NewsSource.active)
  end
end
