# -*- encoding: utf-8 -*-
module ApplicationHelper
  def additional_javascript_permitted?
    Rails.env == 'production' && !permitted_to?(:be_free, :from_tracking)
  end

  def published_date(thing)
    if thing.created_at > DateTime.now - 3
      time_ago_in_words(thing.created_at) + ' назад'
    elsif thing.created_at.year == DateTime.now.year
      l thing.created_at, format: :ru_short_date
    else
      l thing.created_at, format: :ru_full_date
    end
  end

  def site_title
    default_title = 'Новости Дагестана | Кавигатор'
    if @title
      "#{@title} | #{default_title}"
    elsif @trend
      "#{@trend.name} | #{default_title} "
    elsif @news_source
      "#{@news_source.name} | #{default_title} "
    elsif @article
      "#{@article.title} | #{default_title}"
    else
      default_title
    end
  end

  def current_background_url
    # default_background = 'http://logbook04-maskodook.rhcloud.com/content/images/2014/Jan/background_cover_preview.jpg'
    asset_path(random_background_url)
  end

  def random_background_url
    file = Dir[Rails.root.join('app', 'assets', 'images', 'backgrounds', background_folder, '*.jpg')].sample
    "backgrounds/#{background_folder}/#{File.basename(file)}"
  end

  def background_folder
    current_time = Time.current
    if (8..16).include?(current_time.hour)
      'day'
    elsif (17..22).include?(current_time.hour)
      'evening'
    else
      'night'
    end
  end
end
