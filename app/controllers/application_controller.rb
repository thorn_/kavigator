class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  filter_access_to :all unless Rails.env == 'test'

  before_filter :find_trends

  def find_trends
    @trends = Trend.active
    @admin_trends = Trend.order('active DESC, created_at DESC')
  end
end
