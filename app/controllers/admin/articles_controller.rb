class Admin::ArticlesController < Admin::BaseController
  def index
    params[:published] ||= false
    @articles = ArticleFilter.filter(params, params[:published])
    @progresses = FeedProgress.active
  end

  def update
    @article = Article.find(params[:id])
    @article.update_attributes(article_params)
    render nothing: true
  end

  def destroy
    @article = Article.find(params[:id])
    @article.destroy
    render nothing: true
  end

  def feed
    NewsFactory.get_news(NewsSource.active)
    redirect_to admin_articles_path
  end

  private

  def article_params
    params.require(:article).permit!
  end
end
