# -*- encoding: utf-8 -*-
class Admin::KeywordsController < Admin::BaseController
  before_filter :find_keyword, only: [:edit, :update, :destroy]
  def index
    @keywords = Keyword.all
  end

  def new
    @keyword = Keyword.new
  end

  def create
    @keywords = []
    error = false
    error = true if params[:keyword][:name].empty?
    params[:keyword][:name].split("\n").each do |n|
      @keyword = Keyword.new(keyword_params.merge(name: n.chomp.strip))
      error = true unless @keyword.valid? || error
      @keywords << @keyword
    end

    if error
      @keyword ||= Keyword.new
      flash.now[:error] = 'Ключевые слова не созданы.'
      render :new
    else
      @keywords.each(&:save)
      redirect_to admin_keywords_path, flash: { success: 'Ключевые слова успешно созданы.' }
    end
  end

  def edit
  end

  def update
    if @keyword.update_attributes(keyword_params)
      redirect_to admin_keywords_path, flash: { success: 'Ключевое слово успешно изменено.' }
    else
      flash.now[:error] = 'Ключевое слово не изменено.'
      render :edit
    end
  end

  def destroy
    @keyword.destroy
    redirect_to admin_keywords_path, flash: { success: 'Ключевое слово успешно удалено.' }
  end

  private

  def find_keyword
    @keyword = Keyword.find(params[:id])
  end

  def keyword_params
    params.require(:keyword).permit(:name, :keyword_type)
  end
end
