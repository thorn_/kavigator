# -*- encoding: utf-8 -*-
class Admin::UsersController < Admin::BaseController
  before_filter :find_user, only: [:edit, :update, :destroy]

  def index
    @users = User.all
  end

  def destroy
    @user.destroy
    redirect_to admin_users_path, flash: { success: 'Пользователь успешно удален.' }
  end

  def edit
  end

  def new
    @user = User.new
  end

  def update
    parameters = user_params
    if user_params[:password].blank?
      parameters.delete(:password)
      parameters.delete(:password_confirmation)
    end
    @user.update_attributes!(parameters)
    redirect_to admin_users_path, flash: { success: 'Роль пользователя успешно изменена.' }
  end

  def create
    @user = User.new(user_params)
    if @user.save
      redirect_to admin_users_path, flash: { success: 'Новый пользователь успешно создан.' }
    else
      render :new
    end
  end

  private

  def find_user
    @user = User.find(params[:id])
  end

  def user_params
    params.require(:user).permit(:email, :password, :password_confirmation, :role)
  end
end
