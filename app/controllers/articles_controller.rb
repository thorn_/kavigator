# -*- encoding: utf-8 -*-
class ArticlesController < ApplicationController
  def index
    @articles = ArticleFilter.filter(params)
    @today_news_count = Article.where('created_at > ?', Time.now.midnight).count
    @news_source = NewsSource.find_by_id(params[:source_id])
    @trend = Trend.find_by_id(params[:trend_id])
  end

  def feed
    @title = 'Новости Дагестана Кавигатор'
    @articles = ArticleFilter.filter(params)
    @updated = @articles.first.updated_at unless @articles.empty?
    respond_to do |format|
      format.html
      format.rss { redirect_to feed_articles_path(format: :atom), status: :moved_permanently }
      format.atom { render layout: false }
    end
  end

  def show
    @article = Article.find(params[:id])
  end
end
