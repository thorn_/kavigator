#= require jquery
#= require jquery_ujs
#= require chosen-jquery
$ ->
  # infinitie scrolling
  $(window).scroll ->
    url = $('.pagination .next_page').attr('href')
    if url and $(window).scrollTop() > $(document).height() - $(window).height() - 50
      $('.pagination').html('<h3>Загружаем новости...</h3>')
      $.getScript(url)

  $('.choosen_select').chosen
    allow_single_deselect: true
    no_results_text: 'No results matched'
    width: '200px'
    placeholder_text: '\0'
    placeholder_text_multiple: '\0'
    display_selected_options: false

  # Trending
  $("body").on 'change', '#article_sticky, #article_published', (ev)->
    article_id = parseInt($(this).parents('*[id^=article_]').attr('id').split('_')[1])
    if $(ev.target).attr('type') is 'checkbox'
      value = if this.checked then 'true' else 'false'
      $(ev.target).parents('.article').hide()
    else
      value = $(ev.target).val()
    input_name = 'article[' + $(ev.target).attr('id').split('article_')[1] + ']'
    data = {}
    data[input_name] = value
    $.ajax
      url: "/admin/articles/#{article_id}"
      type: 'PUT'
      data: data
      success: (result) ->
        console.log(result)

      error: (result) ->
        console.log(result)
    false

  $('body').on 'click', '.edit_article_attribute_link', (ev) ->
    target = $(ev.currentTarget)
    url = target.attr('href')
    attribute = target.data('attribute')
    value = target.data('value')
    method = target.data('method') || 'post'
    data = {}
    data[attribute] = value
    $.ajax
      url: url
      type: method
      data: data
      success: (result) ->
        console.log(result)

      error: (result) ->
        console.log(result)
    return false

  # fix for tablets not showing dropdown on touch
  $('a.dropdown-toggle, .dropdown-menu a, .btn-group').on 'touchstart', (e) ->
    e.stopPropagation()

  $('body').on 'click', '.delete_article, .publish_article, .unpublish_article',->
    $(this).parents('*[id^=article_]').first().remove()

  $('body').on 'click', '.sticky', (e) ->
    $(e.currentTarget).addClass('hidden')
    $(e.currentTarget).siblings('.un_sticky').removeClass('hidden')
    return false

  $('body').on 'click', '.un_sticky', (e) ->
    $(e.currentTarget).addClass('hidden')
    $(e.currentTarget).siblings('.sticky').removeClass('hidden')
    return false
# $(document).ready ->
#   window.fix_images()

# window.fix_images = ->
#   $("img.article_image").load ->
#     container_width = 82
#     image_width = this.width
#     margin = Math.floor((this.width - container_width) / 2)
#     sign = if margin > 0 then '-' else ''
#     $(this).css('margin-left', "#{sign}#{Math.abs(margin)}px")
