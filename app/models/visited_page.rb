class VisitedPage < ActiveRecord::Base
  validates_presence_of :url
  belongs_to :news_source
end
