class Keyword < ActiveRecord::Base
  enum keyword_type: [:required, :not_included]

  validates_presence_of :name, :keyword_type

  scope :required,     -> { where(keyword_type: 0) }
  scope :not_included, -> { where(keyword_type: 1) }
end
