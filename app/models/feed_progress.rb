class FeedProgress < ActiveRecord::Base
  scope :active, -> { where(active: true) }

  def self.start(total, current = 0)
    FeedProgress.create(
      active: true,
      started_at: DateTime.current,
      total: total,
      current: current
    )
  end

  def finish
    update_column(:active, false)
  end

  def progress
    progress = ((current / total.to_f).round(2) * 100).round
    finish if progress == 100 && active?
    progress
  end

  def update_progress(current)
    update_column(:current, current)
  end
end
