class Trend < ActiveRecord::Base
  has_many :articles
  validates_presence_of :name, :description

  scope :active, -> { where(active: true) }
end
