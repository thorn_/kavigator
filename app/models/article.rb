# -*- encoding: utf-8 -*-
require 'news_factory'
class Article < ActiveRecord::Base
  scope :published,   -> { where(published: true) }
  scope :unpublished, -> { where(published: false) }

  belongs_to :news_source
  belongs_to :trend
  validates_presence_of :text, :title, :news_source_id
end
