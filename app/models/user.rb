# --- encoding: utf-8 ---
class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, # :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  validates_presence_of :role

  def role_symbols
    user_role = role
    user_role ? [user_role.to_sym] : [:guest]
  end

  def online?
    updated_at >= 5.minutes.ago
  end

  USER_ROLES = {
    'admin' => 'Администратор',
    'user' => 'Пользователь',
    'moderator' => 'Модератор'
  }

  def user_role
    USER_ROLES[role]
  end
end
