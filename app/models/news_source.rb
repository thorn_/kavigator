class NewsSource < ActiveRecord::Base
  validates_presence_of :name, :description
  validates :url,
            presence: true,
            uniqueness: true,
            format: %r{\A(http://|https://|\S)[a-zA-Z0-9\-\_\.]+\.[a-zA-Z]{2,3}(/\S*)?\z}i
  validates :webpage,
            presence: true,
            format: %r{\A(http://|https://)[a-zA-Z0-9\-\_\.]+\.[a-zA-Z]{2,4}(/\S*)?\z}i

  has_many :articles, dependent: :destroy
  has_many :visited_pages, dependent: :destroy

  scope :active, -> { where(active: true) }

  def clear
    articles.delete_all
    visited_pages.delete_all
  end
end
