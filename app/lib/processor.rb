# -*- encoding: utf-8 -*-
require 'nokogiri'

module Processor
  class Processor
    def initialize(options)
      @soup = options[:soup]
    end

    def process
      fail 'Abstract method was called'
    end
  end
end
