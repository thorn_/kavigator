# -*- encoding: utf-8 -*-
require 'nokogiri'

class Page
  attr_reader :text, :url

  PROCESSORS = [
    Processor::ImageProcessor,
    Processor::IgnoredProcessor,
    Processor::AttributesProcessor,
    Processor::LinksProcessor
  ]

  def initialize(url)
    @url = url
    @text = get_page
  end

  def image
    html.at_css('img') && html.at_css('img')['src']
  end

  def summary
    if first_paragraph = html.at_css('p')
      first_paragraph.text
    elsif first_div = html.at_css('div')
      first_div.text
    else
      ''
    end
  end

  def process!(options = {})
    soup = html.css(options[:find_helper])
    PROCESSORS.each do |p|
      processor = p.new(options.merge(soup: soup, host: host))
      processor.process
    end
    @text = soup.to_s.encode('UTF-8')
    self
  end

  def check
    checkers = [
      Checker::ArticleLengthChecker,
      Checker::RequiredWordsChecker,
      Checker::ExcludedWordsChecker
    ]
    checkers.each do |checker|
      ch = checker.new(text)
      return false unless ch.check
    end
    true
  end

  private

  def host
    URI.parse(URI.encode(url)).host
  end

  def html
    Nokogiri::HTML(@text)
  end

  def get_page
    request = Curl::Easy.perform(url) do |curl|
      curl.follow_location = true
      curl.max_redirects = 3
      curl.timeout = 5
      curl.headers['User-Agent'] = 'Mozilla/5.0 (X11; Linux i686) \
      AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.77 Safari/535.7'
    end
    Nokogiri::HTML(request.body_str).to_s.encode('UTF-8')
  rescue Curl::Err::TimeoutError,
         Curl::Err::HostResolutionError,
         URI::InvalidURIError,
         Nokogiri::CSS::SyntaxError
    return ''
  end
end
