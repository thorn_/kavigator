class Entry
  attr_reader :entry
  extend Forwardable
  def_delegators :entry, :url, :title
  def initialize(entry)
    @entry = entry
  end

  def summary
    if entry.summary && entry.summary.strip.length > 0
      Nokogiri::HTML(entry.summary).text[0..300]
    end
  end
end
