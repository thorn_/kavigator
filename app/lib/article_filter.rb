class ArticleFilter
  def self.filter(params = {}, published = true)
    q = {
      trend_id_eq: params[:trend_id],
      archive_eq: false,
      published_eq: published,
      news_source_id_eq: params[:source_id]
    }
    q = {} if params[:archive]
    query = Article.ransack(q)
    query.result.order(created_at: :desc).page(params[:page]).per_page(25)
  end
end
