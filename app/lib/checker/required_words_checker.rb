module Checker
  class RequiredWordsChecker < BaseChecker
    def initialize(params)
      super params
    end

    def check(_params = {})
      Keyword.required.each do |word|
        return true if word_on_text(word.name) > 0
      end
      false
    end
  end
end
