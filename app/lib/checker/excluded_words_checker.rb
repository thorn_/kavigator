module Checker
  class ExcludedWordsChecker < BaseChecker
    def initialize(params)
      super params
    end

    def check(_params = {})
      Keyword.not_included.each do |word|
        return false if word_on_text(word.name) > 0
      end
      true
    end
  end
end
