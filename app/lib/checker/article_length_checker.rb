module Checker
  class ArticleLengthChecker < BaseChecker
    def initialize(options)
      super options
    end

    def check(params = {})
      length = params.fetch(:length, 30)
      word_count > length
    end
  end
end
