require 'lingua/stemmer'

module Checker
  class BaseChecker
    def initialize(text)
      @text = Nokogiri::HTML(text).text
    end

    def check(_params = {})
      fail 'Abstract method called'
    end

    def word_on_text(word)
      @text.scan(/#{word}/i).length
    end

    def word_count
      stemmer = Lingua::Stemmer.new(language: 'ru')
      @text.split(/[^A-Za-zА-Яа-я]+/i)
        .select { |w| w.length > 3 && w.length < 20 }
        .uniq
        .collect { |w| stemmer.stem(w.strip.chomp.downcase.to_s) }.uniq.length
    end
  end
end
