class Processor::IgnoredProcessor < Processor::Processor
  def initialize(options)
    super options
    @ignored = options[:ignored_helper] || 'ignored'
    @blacklist = "#{@ignored}, script, style, link, iframe, textarea, input"
  end

  def process
    @soup.css(@blacklist).remove
    @soup
  end
end
