class Processor::ImageProcessor < Processor::Processor
  def initialize(options = { host: 'http://kavigator.ru' })
    super options
    @host = options[:host]
    @injections = [
      'новости дагестана',
      'новости дагестана сегодня',
      'новости дагестана последние',
      "новости дагестан #{Date.today.year}",
      'новости дагестана свежие',
      'риа новости дагестан сегодня',
      'криминальные новости дагестана',
      'новости дагестана криминал',
      'дагестан буйнакск новости',
      'дагестан новости махачкале',
      'новости дагестана хасавюрт',
      'новости республики дагестан',
      'новости дня в дагестане']
  end

  def process
    @soup.css('img').each do |img|
      if img['src']
        img['src'] = 'http://' + @host + img['src'] if img['src'][0] == '/' && img['src'][0..1] != '//'
        img['alt'] = (img['alt'] || '') + '. ' + @injections.sample
        img['rel'] = 'nofollow'
      else
        img.remove
      end
    end
    @soup
  end
end
