class Processor::LinksProcessor < Processor::Processor
  def initialize(options)
    super options
    @host = options[:host] || 'http://kavigator.ru'
  end

  def process
    @soup.css('a').each do |link|
      if link['href']
        next if link['href'][0] == '#'
        link['target'] = '_blank'
        link['href'] = 'http://' + @host + link['href'] if link['href'][0] == '/'
      else
        link.remove unless link['name']
      end
    end
    @soup
  end
end
