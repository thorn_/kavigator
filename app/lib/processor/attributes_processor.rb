class Processor::AttributesProcessor < Processor::Processor
  def initialize(options)
    super options
    @blacklist = %w(style width height class) + (options[:ignored_attributes] || [])
  end

  def process
    @blacklist.each do |attribute|
      @soup.css("[#{attribute}]").each { |el| el.remove_attribute(attribute) }
    end
    @soup
  end
end
