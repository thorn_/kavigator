# -*- encoding: utf-8 -*-
require 'timeout'
require 'checker'
require 'pry'
module NewsFactory
  def self.get_news(news_sources)
    progress = FeedProgress.start(news_sources.count || 1)
    current_progress = 0
    news_sources.each do |source|
      progress.update_progress(current_progress += 1)

      feed(source.url).entries.each do |entry|
        next if source.articles.find_by_url(entry.url)
        next if source.visited_pages.find_by_url(entry.url)

        entry = Entry.new(entry)
        page = process_page(entry.url, source.find_helper, source.ignore_helper)
        if (source.greedy? && !page.text.strip.empty?) || page.check
          create_article(page, entry, source)
        end
        source.visited_pages.create(url: entry.url)
      end
    end
    progress.finish
  end

  def self.feed(url)
    Feedjira::Feed.fetch_and_parse(url)
  rescue Feedjira::FetchFailure, Feedjira::NoParserAvailable
    []
  end

  def self.process_page(url, find_helper, ignore_helper)
    page = Page.new(url)
    page.process!(find_helper: find_helper,ignored_helper: ignore_helper)
  end

  def self.create_article(page, entry, source)
    Article.create!(
      title: entry.title,
      url: page.url,
      img_url: page.image,
      text: page.text,
      summary: entry.summary || page.summary[0..300],
      news_source_id: source.id
    )
  end
end
