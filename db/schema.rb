# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150809173919) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "articles", force: :cascade do |t|
    t.text     "title"
    t.text     "text"
    t.text     "summary"
    t.text     "url"
    t.text     "img_url"
    t.integer  "news_source_id"
    t.integer  "trend_id"
    t.string   "ancestry"
    t.boolean  "published",      default: false
    t.boolean  "sticky",         default: false
    t.boolean  "shared",         default: false
    t.boolean  "archive",        default: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "articles", ["ancestry"], name: "index_articles_on_ancestry", using: :btree
  add_index "articles", ["archive"], name: "index_articles_on_archive", using: :btree
  add_index "articles", ["news_source_id"], name: "index_articles_on_news_source_id", using: :btree
  add_index "articles", ["trend_id"], name: "index_articles_on_trend_id", using: :btree

  create_table "feed_progresses", force: :cascade do |t|
    t.boolean  "active",      default: true
    t.integer  "total"
    t.integer  "current",     default: 0
    t.datetime "started_at"
    t.datetime "finished_at"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "keywords", force: :cascade do |t|
    t.string   "name",                     null: false
    t.integer  "keyword_type", default: 0
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "news_sources", force: :cascade do |t|
    t.string   "name",                               null: false
    t.string   "url",                                null: false
    t.string   "webpage"
    t.string   "image_url"
    t.text     "description"
    t.text     "find_helper",   default: "body",     null: false
    t.text     "ignore_helper", default: "noignore", null: false
    t.boolean  "active",        default: true,       null: false
    t.boolean  "greedy",        default: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "trends", force: :cascade do |t|
    t.string   "name",        default: ""
    t.text     "description"
    t.boolean  "active",      default: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",     null: false
    t.string   "encrypted_password",     default: "",     null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,      null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "role",                   default: "user", null: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "visited_pages", force: :cascade do |t|
    t.string   "url",            null: false
    t.integer  "news_source_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "visited_pages", ["news_source_id"], name: "index_visited_pages_on_news_source_id", using: :btree
  add_index "visited_pages", ["url"], name: "index_visited_pages_on_url", using: :btree

end
