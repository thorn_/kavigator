class CreateVisitedPages < ActiveRecord::Migration
  def change
    create_table :visited_pages do |t|
      t.string :url, null: false, index: true
      t.integer :news_source_id, index: true

      t.timestamps
    end
  end
end
