class CreateArticles < ActiveRecord::Migration
  def change
    create_table :articles do |t|
      t.text :title
      t.text :text
      t.text :summary
      t.text :url
      t.text :img_url
      t.integer :news_source_id
      t.integer :trend_id
      t.string :ancestry
      t.boolean :published, default: false
      t.boolean :sticky, default: false
      t.boolean :shared, default: false
      t.boolean :archive, default: false, index: true

      t.timestamps
    end
    add_index :articles, :news_source_id
    add_index :articles, :trend_id
    add_index :articles, :ancestry
  end
end
