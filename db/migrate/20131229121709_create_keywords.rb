class CreateKeywords < ActiveRecord::Migration
  def change
    create_table :keywords do |t|
      t.string :name, null: false
      t.integer :keyword_type, default: 0

      t.timestamps
    end
  end
end
