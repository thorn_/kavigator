class CreateFeedProgresses < ActiveRecord::Migration
  def change
    create_table :feed_progresses do |t|
      t.boolean :active, default: true
      t.integer :total
      t.integer :current, default: 0
      t.datetime :started_at
      t.datetime :finished_at

      t.timestamps null: false
    end
  end
end
