class CreateNewsSources < ActiveRecord::Migration
  def change
    create_table :news_sources do |t|
      t.string :name, null: false
      t.string :url, null: false
      t.string :webpage
      t.string :image_url
      t.text :description
      t.text :find_helper, null: false, default: 'body'
      t.text :ignore_helper, null: false, default: 'noignore'
      t.boolean :active, null: false, default: true
      t.boolean :greedy, default: false

      t.timestamps
    end
  end
end
