source 'https://rubygems.org'

gem 'rails', '4.2.3'

gem 'sqlite3'
gem 'pg'

gem 'sass-rails', '~> 5.0'
gem 'uglifier', '>= 1.3.0'
gem 'coffee-rails', '~> 4.1.0'
gem 'jquery-rails'
gem 'haml-rails'
gem 'therubyracer', platforms: :ruby

gem 'jbuilder', '~> 2.0'

gem 'bcrypt-ruby', '~> 3.1.5'

group :test do
  gem 'guard'
  gem 'guard-rspec'
  gem 'zeus'
  gem 'rspec-rails'
  gem 'shoulda-matchers'
  gem 'factory_girl'
  gem 'database_cleaner'
  gem 'simplecov', require: false
  gem 'vcr'
  gem 'webmock'
end

gem 'thin'
gem 'quiet_assets'
gem 'hirb'

gem 'bootstrap-sass'
gem 'compass-rails'
gem 'font-awesome-rails'

gem 'curb'
gem 'feedjira'
gem 'ruby-stemmer'
gem 'russian'
gem 'will_paginate'
gem 'devise'
gem 'declarative_authorization'
gem 'chosen-rails'
gem 'sitemap_generator', require: 'sitemap_generator'
gem 'ransack'
gem 'simple_form'
gem 'sidekiq'
gem 'sinatra', :require => nil
gem 'whenever', :require => false
gem 'pry-rails'
gem 'pry-byebug'

# Deployment
gem 'unicorn'
gem 'capistrano'
gem 'capistrano-rails'
gem 'capistrano-bundler'
gem 'capistrano-rails-console'
gem 'capistrano-rvm'
gem 'capistrano-unicorn-nginx', '~> 3.2.0'
gem 'capistrano-sidekiq'
